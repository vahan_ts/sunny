//
//  WeathersViewController.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

protocol IWeathersPageViewLogic: class {
    func dbDidUpdate()
}

class WeathersPageViewController: UIViewController {
    
    private weak var pageViewController: UIPageViewController!
    private var viewModel: IWeathersPageViewModel!
    private var locationTypes = [WeatherDisplayModel.LocationType]()
    
    private var pageIndex: Int? {
        return (self.pageViewController.viewControllers?.first as? WeatherViewController)?.pageIndex
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureViewModel()
        self.configurePageViewController()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "PageViewController":
            self.pageViewController = (segue.destination as? UIPageViewController)
        case "WeathersListViewController":
            (segue.destination as? WeathersListViewController)?.delegate = self
        default:
            break
        }
    }
    
    //MARK: - Private methods
    
    private func configurePageViewController() {
        self.pageViewController.dataSource = self
        self.pageViewController.delegate = self
        self.updateCurrentPage()
    }
    
    private func configureViewModel() {
        let viewModel = WeathersPageViewModel()
        viewModel.view = self
        self.viewModel = viewModel
        self.locationTypes = self.viewModel.getAllLocationTypes()
    }
    
    private func viewController(_ atIndex: Int) -> UIViewController? {
        var index = max(0, atIndex)
        index = min(self.locationTypes.count - 1, index)
        guard index >= 0 else {
            return nil
        }
        let identifier = "WeatherViewControllerID"
        let vc = self.storyboard?.instantiateViewController(withIdentifier: identifier) as! WeatherViewController
        vc.setWeatherLocationType(self.locationTypes[index])
        vc.pageIndex = index
        vc.delegate = self
        return vc
    }
    
    private func updateCurrentPage() {
        self.locationTypes = self.viewModel.getAllLocationTypes()
        let pageIndex = self.pageIndex ?? 0
        self.setCurrentCurrentPage(index: pageIndex)
    }
    
    private func setCurrentCurrentPage(index: Int) {
        if let vc = self.viewController(index) {
            self.pageViewController.setViewControllers([vc], direction: .forward, animated: false, completion: nil)
            self.didChangePage()
        }
    }
    
    private func didChangePage() {
        let model = (self.pageViewController.viewControllers?.first as? WeatherViewController)?.model
        self.view.backgroundColor = WeatherLocationModel.colorSYS(for: model)
    }

}

extension WeathersPageViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let index = self.pageIndex, index - 1 >= 0 else {
            return nil
        }
        return self.viewController(index - 1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let index = self.pageIndex, index + 1 < self.locationTypes.count else {
            return nil
        }
        return self.viewController(index + 1)
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return self.locationTypes.count
    }

    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return self.pageIndex ?? 0
    }
    
}

extension WeathersPageViewController: UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        self.didChangePage()
    }
    
}

extension WeathersPageViewController: WeathersListViewControllerDelegate {
    
    func didSelect(listViewController: WeathersListViewController, modelID: Int) {
        let index = self.locationTypes.firstIndex(where: {
            switch $0 {
            case .locationModel(let model):
                return model.id == modelID
            default:
                return false
            }
        }) ?? 0
        self.setCurrentCurrentPage(index: index)
        
    }
    
}

extension WeathersPageViewController: IWeathersPageViewLogic {
    
    func dbDidUpdate() {
        self.updateCurrentPage()
    }
    
}

extension WeathersPageViewController: WeatherViewControllerDelegate {
    
    func didChange(viewController: WeatherViewController, model: WeatherLocationModel?) {
        if viewController.pageIndex == self.pageIndex {
            self.view.backgroundColor = WeatherLocationModel.colorSYS(for: model)
        }
    }
    
}


//
//  WeathersViewModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol IWeathersPageViewModel: class {
    func getAllLocationTypes() -> [WeatherDisplayModel.LocationType]
}

class WeathersPageViewModel {
    
    private let weatherService = WeatherService.weatherService
    weak var view: IWeathersPageViewLogic?
    
    init() {
        self.weatherService.addObserver(obj: self)
    }
        
}

extension WeathersPageViewModel: IWeathersPageViewModel {
    
    func getAllLocationTypes() -> [WeatherDisplayModel.LocationType]  {
        guard self.weatherService.isReady else {
            return []
        }
        var result: [WeatherDisplayModel.LocationType] = [.currentLocation]
        var currentLocationID: Int?
        switch self.weatherService.currentLocationStatus {
        case .success(let id):
            currentLocationID = id
        default:
            break
        }
        self.weatherService.locationModels.forEach { (model) in
            if model.id != currentLocationID {
                result.append(.locationModel(model: model))
            }
        }
        return result
    }
    
}

extension WeathersPageViewModel: IWeatherServiceObserver {
    
    func didAdd(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {
        self.view?.dbDidUpdate()
    }
    
    func didDelete(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {
        self.view?.dbDidUpdate()
    }
    
    func didReady(service: IWeatherService) {
        self.view?.dbDidUpdate()
    }
    
    func didChange(service: IWeatherService, currentLocation status: WeatherService.CurrentLocationStatus) {
        self.view?.dbDidUpdate()
    }
    
}

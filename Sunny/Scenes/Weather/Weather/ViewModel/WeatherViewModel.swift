//
//  WeatherViewControllerViewModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/11/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

protocol IWeatherViewModel: class {
    var weatherLocationModel: WeatherLocationModel? { get }
    func reload()
    func reload(locationType: WeatherDisplayModel.LocationType)
}


class WeatherViewModel {
        
    weak var view: IWeatherViewLogic?
    private var weatherLocation: WeatherLocationModel?
    private var locationType: WeatherDisplayModel.LocationType
    
    private let weatherService: IWeatherService
    private var requestWeatherCurrentLocationSuccess: ((WeatherLocationModel) -> Void)?
    private var requestWeatherCurrentLocationFailure: ((Error) -> Void)?
                
    init(locationType: WeatherDisplayModel.LocationType, service: IWeatherService = WeatherService.weatherService) {
        self.locationType = locationType
        self.weatherService = service
        self.weatherService.addObserver(obj: self)
    }
    
    private func requestWeather(success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) {
        switch self.locationType {
        case .name(let name):
            self.weatherService.requestWeather(name: name, success: success, failure: failure)
        case .coordinats(let longitude, let latitude):
            self.weatherService.requestWeather(longitude: longitude, latitude: latitude, success: success, failure: failure)
        case .currentLocation:
            self.requestWeatherCurrentLocation(success: success, failure: failure)
        case .locationModel(let model):
            success(model)
        }
    }
    
    private func requestWeatherCurrentLocation(success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) {
        guard let currentLocationStatus = self.weatherService.currentLocationStatus else {
            self.requestWeatherCurrentLocationSuccess = success
            self.requestWeatherCurrentLocationFailure = failure
            return
        }
        switch currentLocationStatus {
        case .success(let id):
            guard let model = self.weatherService.weatherLocationModel(by: id) else {
                failure(NetworkDispatcher.NetworkError.notFound)
                return
            }
            success(model)
        case .error(let error):
            failure(error)
        }
    }
    
    private func update(for weatherLocation: WeatherLocationModel) {
        guard self.hasChanges(weatherLocation: weatherLocation) else {
            return
        }
        self.weatherLocation = weatherLocation
        self.view?.didRetrieved(model: weatherLocation)
    }
    
    private func didUpdateCurrentLocation(for model: WeatherLocationModel) {
        if let requestWeatherCurrentLocationSuccess = self.requestWeatherCurrentLocationSuccess {
            requestWeatherCurrentLocationSuccess(model)
            self.requestWeatherCurrentLocationFailure = nil
            self.requestWeatherCurrentLocationSuccess = nil
        } else {
            self.view?.didRetrieved(model: model)
        }
    }
    
    private func didUpdateCurrentLocation(for error: Error) {
        if let requestWeatherCurrentLocationFailure = self.requestWeatherCurrentLocationFailure {
            requestWeatherCurrentLocationFailure(error)
            self.requestWeatherCurrentLocationFailure = nil
            self.requestWeatherCurrentLocationSuccess = nil
        } else {
            self.view?.didRetrieved(error: error)
        }
    }
    
    private func hasChanges(weatherLocation: WeatherLocationModel?) -> Bool {
        return weatherLocation?.weathers.count != self.weatherLocation?.weathers.count
    }
    
}

extension WeatherViewModel: IWeatherViewModel {
    
    var weatherLocationModel: WeatherLocationModel? {
        return self.weatherLocation
    }
    
    func reload() {
        self.requestWeather { [weak self] (weatherLocation) in
            self?.update(for: weatherLocation)
        } failure: {[weak self] (error) in
            self?.view?.didRetrieved(error: error)
        }
    }
    
    func reload(locationType: WeatherDisplayModel.LocationType) {
        self.weatherLocation = nil
        self.locationType = locationType
        self.requestWeather { [weak self] (weatherLocation) in
            self?.update(for: weatherLocation)
        } failure: {[weak self] (error) in
            self?.view?.didRetrieved(error: error)
        }
    }
    
}

extension WeatherViewModel: IWeatherServiceObserver {
    
    func didChange(service: IWeatherService, currentLocation status: WeatherService.CurrentLocationStatus) {
        guard self.locationType.workingType == .currentLocation else {
            return
        }
        switch status {
        case .success(let id):
            guard let weatherLocation = service.weatherLocationModel(by: id) else {
                self.didUpdateCurrentLocation(for: NetworkDispatcher.NetworkError.unknown)
                return
            }
            self.didUpdateCurrentLocation(for: weatherLocation)
        case .error(let error):
            self.didUpdateCurrentLocation(for: error)
        }
    }
    
    func didUpdate(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {
        guard let weatherLocation = models.first(where: { $0.id == self.weatherLocation?.id })  else {
            return
        }
        self.update(for: weatherLocation)
    }
    
}

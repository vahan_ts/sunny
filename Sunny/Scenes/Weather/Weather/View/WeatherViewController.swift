//
//  WeatherViewController.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/10/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit


protocol WeatherViewControllerDelegate: class {
    func didChange(viewController: WeatherViewController, model: WeatherLocationModel?)
}

protocol IWeatherViewLogic: class {
    func didRetrieved(model: WeatherLocationModel)
    func didRetrieved(error: Error)
}

class WeatherViewController: UITableViewController {
    
    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var historyLabel: UILabel!
    @IBOutlet private weak var minMaxTemperatureLabel: UILabel!
    @IBOutlet private weak var weatherIconImageView: UIImageView!
    @IBOutlet private weak var infoView: WeatherInfoView!
    @IBOutlet private weak var datesTableViewCell: WeatherDatesTableViewCell!
    
    private var viewModel: IWeatherViewModel?
    private(set) var weatherLocationType: WeatherDisplayModel.LocationType?
    
    var model: WeatherLocationModel? {
        return self.viewModel?.weatherLocationModel
    }
    
    var pageIndex: Int?
    
    weak var delegate: WeatherViewControllerDelegate?
            
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.rowHeight = UITableView.automaticDimension;
        self.tableView.estimatedRowHeight = 44.0;
        self.historyLabel.text = "history".localized
        self.display(model: nil)
        self.configureViewModel()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    //MARK: - Public methods
    
    func setWeatherLocationType(_ weatherLocationType: WeatherDisplayModel.LocationType) {
        self.weatherLocationType = weatherLocationType
        if self.isViewLoaded {
            self.viewModel?.reload(locationType: weatherLocationType)
        }
    }
    
    //MARK: - Private methods
    
    private func configureViewModel() {
        let weatherLocationType = self.weatherLocationType ?? .currentLocation
        let viewModel = WeatherViewModel(locationType: weatherLocationType)
        viewModel.view = self
        self.viewModel = viewModel
        self.viewModel?.reload()
    }
    
    private func display(model: WeatherDisplayModel.Display) {
        self.cityLabel.text = model.cityName
        self.temperatureLabel.text = model.temperature
        self.minMaxTemperatureLabel.text = model.minMaxTemperature
        self.descriptionLabel.text = model.description
        self.infoView.configure(model: model.info)
        self.datesTableViewCell.configure(dates: model.dateInfos)
        self.weatherIconImageView.setImage(imageUrlStr: model.weatherIcon, placeholder: UIImage(named: "ic_sun"))
    }
    
    private func display(model: WeatherLocationModel?) {
        let displayModel = WeatherDisplayModel.generateDisplayModel(weatherLocation: model)
        self.display(model: displayModel)
        self.delegate?.didChange(viewController: self, model: model)
    }

}

extension WeatherViewController: IWeatherViewLogic {
    
    func didRetrieved(model: WeatherLocationModel) {
        self.display(model: model)
    }
    
    func didRetrieved(error: Error) {
        let alert = WeatherDisplayModel.generateAlert(for: error)
        self.display(alert)
    }

}



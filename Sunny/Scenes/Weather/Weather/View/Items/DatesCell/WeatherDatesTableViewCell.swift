//
//  WeatherDatesTableViewCell.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/14/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class WeatherDatesTableViewCell: UITableViewCell {

    @IBOutlet private weak var collectionView: UICollectionView!
    private let cellIdentifier = "cellIdentifier"
    private var dates: [WeatherDisplayModel.Display.DateInfo]?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.confugureCollectionView()
    }
    
    override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize
        size.height = self.collectionView.contentSize.height
        return size
    }
    
    //MARK: - Public methods
    
    func configure(dates: [WeatherDisplayModel.Display.DateInfo]?)  {
        self.dates = dates
        self.collectionView.reloadData()
    }
    
    //MARK: - Private methods
    
    private func confugureCollectionView() {
        self.collectionView.register(UINib(nibName: "WeatherDatesCollectionViewCell", bundle: Bundle.main), forCellWithReuseIdentifier: self.cellIdentifier)
        let flowLayout = (self.collectionView.collectionViewLayout as? UICollectionViewFlowLayout)
        flowLayout?.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        flowLayout?.itemSize = UICollectionViewFlowLayout.automaticSize
    }
        
}

extension WeatherDatesTableViewCell: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dates?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: self.cellIdentifier, for: indexPath) as! WeatherDatesCollectionViewCell
        cell.configure(model: self.dates?[indexPath.row])
        return cell
        
    }
    
    
    
    
}



//
//  WeatherDatesCollectionViewCell.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/14/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class WeatherDatesCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var dateLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            self.contentView.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.contentView.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.contentView.topAnchor.constraint(equalTo: self.topAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }
    
    func configure(model: WeatherDisplayModel.Display.DateInfo?) {
        self.temperatureLabel.text = model?.temperature
        self.imageView.setImage(imageUrlStr: model?.icon, placeholder: UIImage(named: "ic_sun"))
        self.dateLabel.text = model?.date
    }
        
}

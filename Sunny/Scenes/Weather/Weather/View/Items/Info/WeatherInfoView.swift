//
//  WeatherInfoView.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/14/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class WeatherInfoView: UIView {
    
    @IBOutlet private weak var windLabel: UILabel!
    @IBOutlet private weak var windTitleLabel: UILabel!
    
    @IBOutlet private weak var visibilityLabel: UILabel!
    @IBOutlet private weak var visibilityTitleLabel: UILabel!
    
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var humidityTitleLabel: UILabel!
    
    @IBOutlet private weak var pressureLabel: UILabel!
    @IBOutlet private weak var pressureTitleLabel: UILabel!
    
    private weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadNib()
        self.configureTitleLabels()
    }
    
    override var intrinsicContentSize: CGSize {
        return self.containerView.systemLayoutSizeFitting(.zero)
    }
    
    //MARK: - Public methods
    
    func configure(model: WeatherDisplayModel.Display.Info?) {
        self.windLabel.text = model?.wind
        self.visibilityLabel.text = model?.visibility
        self.humidityLabel.text = model?.humidity
        self.pressureLabel.text = model?.pressure
    }
    
    //MARK: - Private methods
    
    private func configureTitleLabels() {
        self.windTitleLabel.text = "wind".localized
        self.visibilityTitleLabel.text = "visibility".localized
        self.humidityTitleLabel.text = "humidity".localized
        self.pressureTitleLabel.text = "pressure".localized
    }
    
    func loadNib() {
        let nibs = Bundle.main.loadNibNamed("WeatherInfoView", owner: self, options: nil)
        guard let containerView = nibs?.first(where: {($0 as? UIView != nil)}) as? UIView else {
            fatalError("WeatherInfoView nib not found")
        }
        self.containerView = containerView
        self.addSubviewFullContent(in: self.containerView)
    }
    
}

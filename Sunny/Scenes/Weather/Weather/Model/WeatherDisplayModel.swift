//
//  WeatherDisplayModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

enum WeatherDisplayModel {
    
    enum LocationType {
        
        enum WorkingType {
            case name
            case coordinats
            case currentLocation
            case locationModel
        }
        
        case name(name: String)
        case coordinats(longitude: Double, latitude: Double)
        case currentLocation
        case locationModel(model: WeatherLocationModel)
        
        var workingType: WorkingType {
            switch self {
            case .name(_):
                return .name
            case .coordinats(_, _):
                return .coordinats
            case .currentLocation:
                return .currentLocation
            case .locationModel(_):
                return . locationModel
            }
        }
        
    }
    
    struct Display {
        
        let cityName: String?
        let temperature: String?
        let description: String?
        let minMaxTemperature: String?
        let weatherIcon: String?
        let info: Info?
        let dateInfos: [DateInfo]
        
        struct Info {
            var wind: String?
            var visibility: String?
            var humidity: String?
            var pressure: String?
        }
        
        struct DateInfo {
            var date: String?
            var icon: String?
            var temperature: String?
        }
        
    }
    
}

extension WeatherDisplayModel {
    
    //MARK: - Public methods
    
    static func generateAlert(for error: Error) -> UIViewController.AlertModel {
        if error is LocationServiceError {
            let title = "alert_location_unauthorized_message".localized
            var actions = [UIViewController.AlertModel.Action]()
            let okAction = UIViewController.AlertModel.Action(title: "ok".localized, style: .default) { () -> Void in
                if let settingsUrl = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpenURL(settingsUrl) {
                    UIApplication.shared.open(settingsUrl)
                }
            }
            actions.append(okAction)
            let cancelAction = UIViewController.AlertModel.Action(title: "cancel".localized, style: .cancel)
            actions.append(cancelAction)
            let alert = UIViewController.AlertModel(title: title, actions: actions)
            return alert
        } else {
            let message = (error as? WeatherAppError)?.message ?? error.localizedDescription
            var alert = UIViewController.AlertModel(title: "error".localized, message: message)
            let okAction = UIViewController.AlertModel.Action(title: "ok".localized, style: .default)
            alert.addAction(action: okAction)
            return alert
        }
    }
    
    static func generateDisplayModel(weatherLocation: WeatherLocationModel?) -> Display {
        let weather = weatherLocation?.weathers.first
        var temperatureStr: String = "_"
        var minMaxTemperature: String = "_"
        
        if let temperature = weather?.info.temperature {
            temperatureStr = "\(Int(temperature))°C"
        }
        
        if let min = weather?.info.minTemp, let max = weather?.info.maxTemp {
            minMaxTemperature = "\(Int(max))°C / \(Int(min))°C"
        }
         
        let description = weather?.descriptionInfos.first?.descriptionInfo ??  "_"
        var weatherIcon: String?
        if let icon = weather?.descriptionInfos.first?.icon {
            weatherIcon = AppConstants.Enviorment.current.imageUrl + icon + "@2x.png"
        }
        
        let name = weatherLocation?.name ?? "_"
        let info = self.generateWeaderInfoModel(weatherModel: weather)
        let dateInfos = self.generateDateInfos(weathers: weatherLocation?.weathers, timeZone: weatherLocation?.countryCode)
        
        return Display(cityName: name,
                       temperature: temperatureStr,
                       description: description,
                       minMaxTemperature: minMaxTemperature,
                       weatherIcon: weatherIcon,
                       info: info,
                       dateInfos: dateInfos)
    }
    
    //MARK: - Private methods
        
    static private func generateWeaderInfoModel(weatherModel: WeatherModel?) -> Display.Info {
        
        var wind = "__"
        var visibility = "__"
        var humidity = "__"
        var pressure = "__"
        
        if let speed = weatherModel?.wind.speed {
            wind = "\(Int(speed))m/s"
        }
        
        if let visibilityValue = weatherModel?.visibility {
            visibility = "\(Int(visibilityValue))"
        }
        
        if let humidityValue = weatherModel?.info.humidity {
            humidity = "\(Int(humidityValue))%"
        }
        
        if let pressureValue = weatherModel?.info.pressure {
            pressure = "\(Int(pressureValue))hPA"
        }
        
        return Display.Info(wind: wind,
                                 visibility: visibility,
                                 humidity: humidity,
                                 pressure: pressure)
        
    }
    
    static private func generateDateInfos(weathers: [WeatherModel]?, timeZone: String?) -> [Display.DateInfo] {
        guard let weathers = weathers, let timeZone = timeZone else {
            return [Display.DateInfo]()
        }
        
        let dateManager = DateManager(timeZone: timeZone)
        let currentDate = Date()
        let nowTimeIntervatl: TimeInterval = 3600
        
        return weathers.map { (model) -> Display.DateInfo in
            var weatherIcon: String?
            if let icon = model.descriptionInfos.first?.icon {
                weatherIcon = AppConstants.Enviorment.current.imageUrl + icon + "@2x.png"
            }
            let temperatureStr = "\(Int(model.info.temperature))°C"
            let dateStr: String = currentDate.timeIntervalSince(model.date) < nowTimeIntervatl ? "now".localized : dateManager.dateToString(date: model.date, withFormate: .YYYY_MM_DD_HH)
            return Display.DateInfo(date: dateStr, icon: weatherIcon, temperature: temperatureStr)
        }
    }
    
}

//
//  WeatherSearchViewModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol IeatherSearchViewModel {
    
    func generateLocationType(for searchType: WeatherSearchDisplayModel.SearchType, searchText: String) -> WeatherDisplayModel.LocationType?
    
}

class WeatherSearchViewModel {
    
    //MARK: - Public methods
        
    //MARK: - Private methods
    
    private func generateLocationType(location searchText: String) -> WeatherDisplayModel.LocationType? {
        let test = String(searchText.filter { !" ".contains($0) })
        let array = test.components(separatedBy: ",")
        guard array.count == 2, let first = array.first, let last = array.last else {
            return nil
        }
        guard let longitude = Double(first), let latitude = Double(last) else {
            return nil
        }
        return WeatherDisplayModel.LocationType.coordinats(longitude: longitude, latitude: latitude)
    }
    
    private func generateLocationType(cityName searchText: String) -> WeatherDisplayModel.LocationType {
        return WeatherDisplayModel.LocationType.name(name: searchText)
    }
    
}


extension WeatherSearchViewModel: IeatherSearchViewModel {
    
    func generateLocationType(for searchType: WeatherSearchDisplayModel.SearchType, searchText: String) -> WeatherDisplayModel.LocationType? {
        switch searchType {
        case .city:
            return self.generateLocationType(cityName: searchText)
        case .location:
            return self.generateLocationType(location: searchText)
        }
    }
    
    
}

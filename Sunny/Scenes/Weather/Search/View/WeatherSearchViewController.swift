//
//  WeatherSearchViewController.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class WeatherSearchViewController: UIViewController {
        
    private var searchController: UISearchController!
    private var searchType: WeatherSearchDisplayModel.SearchType = .city
    private let viewModel:IeatherSearchViewModel = WeatherSearchViewModel()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureSearchController()
        self.navigationItem.title = "search".localized
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SearchResultViewController", let locationType = (sender as? WeatherDisplayModel.LocationType), let destination = (segue.destination as? SearchResultViewController) {
            destination.setLocationType(locationType: locationType)
        }
    }

    //MARK: - Private methods
    
    private func configureSearchController() {
        self.searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchResultsUpdater = self
        self.searchController.searchBar.autocapitalizationType = .none
        self.searchController.searchBar.delegate = self
        self.searchController.searchBar.scopeButtonTitles = WeatherSearchDisplayModel.SearchType.localizedAllCases
        self.searchController.searchBar.placeholder = "search_bar_placeholder_city".localized
        self.navigationItem.searchController = self.searchController
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.searchController.obscuresBackgroundDuringPresentation = true
        self.definesPresentationContext = true
    }

}

extension WeatherSearchViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        //Implementing for suggestion cities
    }
        
}

extension WeatherSearchViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else {
            return
        }
        guard let locationType = self.viewModel.generateLocationType(for: self.searchType, searchText: text) else {
            return
        }
        self.performSegue(withIdentifier: "SearchResultViewController", sender: locationType)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        self.searchType = WeatherSearchDisplayModel.SearchType.allCases[selectedScope]
        switch self.searchType {
        case .city:
            searchBar.placeholder = "search_bar_placeholder_city".localized
            searchBar.textContentType = .none
            searchBar.keyboardType = .default
        case .location:
            searchBar.placeholder = "search_bar_placeholder_location".localized
            searchBar.textContentType = .location
            searchBar.keyboardType = .numbersAndPunctuation
        }
        searchBar.reloadInputViews()
    }
    
}

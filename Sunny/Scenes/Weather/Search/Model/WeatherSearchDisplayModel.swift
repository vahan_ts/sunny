//
//  WeatherSearchDisplayModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

enum WeatherSearchDisplayModel {
    
    enum SearchType: String, CaseIterable {
        case city = "city_name"
        case location = "location"
        
        static var localizedAllCases: [String] {
            return self.allCases.map { (type) -> String in
                return type.rawValue.localized
            }
        }
        
        var localized: String {
            return self.rawValue.localized
        }
    }

}

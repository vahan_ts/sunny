//
//  WeathersListTableViewCell.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class WeathersListTableViewCell: UITableViewCell {

    @IBOutlet private weak var cityLabel: UILabel!
    @IBOutlet private weak var temperatureLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var minMaxTemperatureLabel: UILabel!
    @IBOutlet private weak var weatherIconImageView: UIImageView!
    @IBOutlet private weak var myLocationImageView: UIImageView!
    @IBOutlet private weak var bgView: UIView!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.7 : 1
    }
    
    func configure(item: WeathersListDisplayModel.Display.Item) {
        self.cityLabel.text = item.cityName
        self.temperatureLabel.text = item.temperature
        self.descriptionLabel.text = item.description
        self.minMaxTemperatureLabel.text = item.minMaxTemperature
        self.weatherIconImageView.setImage(imageUrlStr: item.weatherIcon, placeholder: UIImage(named: "ic_sun"))
        self.myLocationImageView.isHidden = !(item.isMyLocation ?? true)
        self.bgView.backgroundColor = item.color
    }
    
}

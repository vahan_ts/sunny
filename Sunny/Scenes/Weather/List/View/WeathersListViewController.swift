//
//  WeathersListViewController.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

protocol WeathersListViewControllerDelegate: class {
    func didSelect(listViewController: WeathersListViewController, modelID: Int)
}

protocol IWeathersListViewLogic: class {
    func dbDidUpdate()
}

class WeathersListViewController: UIViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    weak var delegate: WeathersListViewControllerDelegate?
    
    private var viewModel: IWeathersListViewModel!
    private var model: WeathersListDisplayModel.Display?
    private let cellReuseIdentifier = "cellReuseIdentifier"
       
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureTableView()
        self.configureViewModel()
    }
    
    //MARK: - User action
    
    @IBAction func didSelectCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    //MARK: - Private methods
    
    private func configureViewModel() {
        let viewModel = WeatherListViewModel()
        viewModel.view = self
        self.viewModel = viewModel
        self.reloadData()
    }
    
    private func configureTableView() {
        self.tableView.register(UINib(nibName: "WeathersListTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: self.cellReuseIdentifier)
    }
    
    private func reloadData() {
        let locations = self.viewModel.getAllLocations()
        self.model = WeathersListDisplayModel.generateDisplayModels(weatherLocations: locations)
        self.tableView.reloadData()
    }
    
}

extension WeathersListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let id = self.model!.items[indexPath.row].id {
            self.delegate?.didSelect(listViewController: self, modelID: id)
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return self.model?.items[indexPath.row].isMyLocation == false
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete, let id = self.model?.items[indexPath.row].id {
            self.viewModel?.delete(by: id)
            self.model?.items.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
}

extension WeathersListViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model?.items.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellReuseIdentifier, for: indexPath) as! WeathersListTableViewCell
        cell.configure(item: self.model!.items[indexPath.row])
        return cell
    }

}

extension WeathersListViewController: IWeathersListViewLogic {
    
    func dbDidUpdate() {
        self.reloadData()
    }
    
}



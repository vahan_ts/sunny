//
//  WeathersListViewModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol IWeathersListViewModel {
    func getAllLocations() -> [WeatherLocationModel]
    func delete(by locationModelID: Int)
}

class WeatherListViewModel {
    
    private var weatherService = WeatherService.weatherService
    weak var view: IWeathersListViewLogic?
    
    init() {
        self.weatherService.addObserver(obj: self)
    }
    
}

extension WeatherListViewModel: IWeathersListViewModel {
    
    func getAllLocations() -> [WeatherLocationModel] {
        guard self.weatherService.isReady else {
            return []
        }
        var result = [WeatherLocationModel]()
        var currentLocationID: Int?
        switch self.weatherService.currentLocationStatus {
        case .success(let id):
            currentLocationID = id
        default:
            break
        }
        self.weatherService.locationModels.forEach { (model) in
            if model.id == currentLocationID {
                result.insert(model, at: 0)
            } else {
                result.append(model)
            }
        }
        return result
    }
    
    func delete(by locationModelID: Int) {
        self.weatherService.delete(by: locationModelID)
    }
    
    
    
}

extension WeatherListViewModel: IWeatherServiceObserver {
    
    func didUpdate(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {
        self.view?.dbDidUpdate()
    }
}



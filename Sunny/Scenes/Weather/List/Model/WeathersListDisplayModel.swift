//
//  WeathersListDisplayModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

enum WeathersListDisplayModel {
    
    struct Display {
        
        var items: [Item]
        
        struct Item {
            let cityName: String?
            let temperature: String?
            let description: String?
            let minMaxTemperature: String?
            let weatherIcon: String?
            let isMyLocation: Bool?
            let id: Int?
            let color: UIColor?
        }
    }
  
}

extension WeathersListDisplayModel {
    
    static func generateDisplayItem(weatherLocation: WeatherLocationModel?) -> Display.Item {
        let weather = weatherLocation?.weathers.first
        var temperatureStr: String = "_"
        var minMaxTemperature: String = "_"
        
        if let temperature = weather?.info.temperature {
            temperatureStr = "\(Int(temperature))°C"
        }
        
        if let min = weather?.info.minTemp, let max = weather?.info.maxTemp {
            minMaxTemperature = "\(Int(max))°C / \(Int(min))°C"
        }
         
        let description = weather?.descriptionInfos.first?.descriptionInfo ??  "_"
        var weatherIcon: String?
        if let icon = weather?.descriptionInfos.first?.icon {
            weatherIcon = AppConstants.Enviorment.current.imageUrl + icon + "@2x.png"
        }
        let name = weatherLocation?.name ?? "_"
        let color = WeatherLocationModel.colorSYS(for: weatherLocation)
        return Display.Item(cityName: name,
                       temperature: temperatureStr,
                       description: description,
                       minMaxTemperature: minMaxTemperature,
                       weatherIcon: weatherIcon,
                       isMyLocation: weatherLocation?.isMyLocation,
                       id: weatherLocation?.id,
                       color: color)
    }
    
    static func generateDisplayModels(weatherLocations: [WeatherLocationModel]?) -> Display {
        let items = weatherLocations?.map({ model -> Display.Item in
            return self.generateDisplayItem(weatherLocation: model)
        })
        return Display(items: items ?? [])
    }

    
}

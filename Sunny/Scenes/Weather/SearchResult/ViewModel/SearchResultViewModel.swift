//
//  SearchResultViewModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol ISearchResultViewModel {
    func isExistWeatherLocationModel(by ID: Int) -> Bool
    func add(weatherLocationModel: WeatherLocationModel)
}

class SearchResultViewModel {
    
    private var weatherService = WeatherService.weatherService
    
}

extension SearchResultViewModel: ISearchResultViewModel {
    
    func isExistWeatherLocationModel(by ID: Int) -> Bool {
        return self.weatherService.weatherLocationModel(by: ID) != nil
    }
     
    func add(weatherLocationModel: WeatherLocationModel) {
        self.weatherService.add(weatherLocationModel)
    }
    
}



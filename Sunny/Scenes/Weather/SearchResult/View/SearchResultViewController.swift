//
//  SearchResultViewController.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

class SearchResultViewController: UIViewController {
    
    private weak var weatherViewController: WeatherViewController?
    
    private var weatherLocationType: WeatherDisplayModel.LocationType?
    private var viewModel: ISearchResultViewModel = SearchResultViewModel()
    private var model: WeatherLocationModel?
    
    @IBOutlet weak var addTOListButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addTOListButton.setTitle("add_to_list".localized, for: .normal)
    }
    
    //MARK: - User Action
    
    @IBAction private func didSelectCloseButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction private func didSelectAddToListButton(_ sender: Any) {
        guard let model = self.model else {
            return
        }
        self.viewModel.add(weatherLocationModel: model)
        self.dismiss(animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "WeatherViewController", let weatherLocationType = self.weatherLocationType {
            self.weatherViewController = (segue.destination as? WeatherViewController)
            self.weatherViewController?.delegate = self
            self.weatherViewController?.setWeatherLocationType(weatherLocationType)
        }
    }
    
    //MARK: - Public methods
    
    func setLocationType(locationType: WeatherDisplayModel.LocationType) {
        self.weatherLocationType = locationType
        self.weatherViewController?.setWeatherLocationType(locationType)
    }

}


extension SearchResultViewController: WeatherViewControllerDelegate {
    
    func didChange(viewController: WeatherViewController, model: WeatherLocationModel?) {
        self.model = model
        self.view.backgroundColor = WeatherLocationModel.colorSYS(for: model)
        self.addTOListButton.isEnabled = model != nil
        if let id = model?.id {
            self.addTOListButton.isHidden = self.viewModel.isExistWeatherLocationModel(by: id)
        } else {
            self.addTOListButton.isHidden = true
        }
    }
    
}

//
//  Observer.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

class Observer<T> {

    let weakRefarray = NSPointerArray.weakObjects()
    
    func addObject(_ listener: T) {
        let pointer = Unmanaged.passUnretained(listener as AnyObject).toOpaque()
        self.weakRefarray.addPointer(pointer)
    }
    
    func removeObject(_ listener: T) {
        for (index, object) in self.objects.enumerated() {
            if (object as AnyObject) === (listener as AnyObject) {
                self.weakRefarray.removePointer(at: index)
                break
            }
        }
    }
    
    var objects: [T] {
        return self.weakRefarray.allObjects as! [T]
    }
    
    func forEach(_ body: (T) throws -> Void) rethrows {
        try self.objects.forEach(body)
    }
    
}

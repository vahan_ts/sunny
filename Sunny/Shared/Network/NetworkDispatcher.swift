//
//  NetworkDispatcher.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/12/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol IEnviorment {
    var baseUrl: String { get }
    var parameters: [String: Any]? { get }
}

class NetworkDispatcher {
    
    typealias NetworkSuccess<T> = (_ result: T) -> Void
    typealias NetworkFailure = (_ error: Error) -> Void
    
    private let session: URLSession
    private let jsonDecoder: IJSONDecoder
    
    init(session: URLSession = URLSession.shared, jsonDecoder: IJSONDecoder = NetworkJSONDecoder()) {
        self.session = session
        self.jsonDecoder = jsonDecoder
    }
    
    //MARK: - Public methods
    
    func request<R: IDecodableRequest>(request: R, session: URLSession? = nil, jsonDecoder: JSONDecoder? = nil, success: @escaping NetworkSuccess<R.DecodableType>, failure: @escaping NetworkFailure) -> URLSessionDataTask? {
        let jsonDecoder = jsonDecoder ?? self.jsonDecoder
        do {
            let urlRequest = try self.generateURLRequest(request: request)
            return self.request(request: urlRequest, session: session, success: { (data) in
                do {
                    let result = try jsonDecoder.decodeData(R.DecodableType.self, from: data)
                    success(result)
                } catch {
                    failure(error)
                }
            }, failure: failure)
        } catch {
            failure(error)
            return nil
        }
    
    }
    
    func request(request: URLRequest, session: URLSession? = nil, success: @escaping NetworkSuccess<Data>, failure: @escaping NetworkFailure) -> URLSessionDataTask {
        let session = session ?? self.session
        let task = session.dataTask(with: request, completionHandler: {data, response, responseError in
            DispatchQueue.main.async {
                do {
                    let validateData = try Self.validateResponse(data: data, response: response, error: responseError)
                    success(validateData)
                } catch {
                    failure(error)
                }
            }
        })
        task.resume()
        return task
    }
    
    //MARK: - Private methods
    
    private func generateURLRequest(request: IRequest) throws -> URLRequest {
        let urlString = request.enviorment.baseUrl + request.path
        var httpBody: Data?
        guard var urlComponents = URLComponents(string: urlString) else {
            throw NetworkError.incorrectPath
        }
        let queryItems = request.enviorment.parameters?.map({ (elem) -> URLQueryItem in
            return URLQueryItem(name: elem.key, value: "\(elem.value)")
        }) ?? []
        
        urlComponents.queryItems = queryItems
        switch request.encoding {
        case .query:
            request.parameters?.forEach({ (elem) in
                let item = URLQueryItem(name: elem.key, value: "\(elem.value)")
                urlComponents.queryItems?.append(item)
            })
        case .default:
            if let parameters = request.parameters {
                httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted)
            }
            guard request.method != .get else {
                throw NetworkError.getRequestHaventBody
            }
        }
        guard let url = urlComponents.url else {
            throw NetworkError.incorrectUrl
        }
        var urlRequest = URLRequest(url: url)
        urlRequest.httpBody = httpBody
        urlRequest.allHTTPHeaderFields = request.headers
        urlRequest.httpMethod = request.method.rawValue
        return urlRequest
    }

    private class func validateResponse(data: Data?, response: URLResponse?, error: Error?) throws -> Data {
        guard let response = response as? HTTPURLResponse, let data = data else {
            throw NetworkError.networkUnreachable
        }
        let statusCode = StatusCode(code: response.statusCode)
        guard statusCode.isValid else {
            throw NetworkError.badRequest(statusCode: response.statusCode)
        }
        return data
    }

}

extension NetworkDispatcher {
    
    private struct StatusCode {
        let code: Int
        var isValid: Bool {
            return code >= 200 && code <= 299
        }
    }
    
    enum NetworkError: WeatherAppError {
        case incorrectUrl
        case incorrectPath
        case getRequestHaventBody
        case networkUnreachable
        case notFound
        case unknown
        case badRequest(statusCode: Int)
        
        var message: String {
            switch self {
            case .incorrectUrl:
                return "error_incorrect_url".localized
            case .incorrectPath:
                return "error_incorrect_path".localized
            case .getRequestHaventBody:
                return "error_get_request_have_not_body".localized
            case .networkUnreachable:
                return "error_network_unreachable".localized
            case .badRequest(_):
                //FIXME: Check Status Code
                return "error_bad_request".localized
            case .unknown:
                return "error_unknown".localized                
            case .notFound:
                return "error_not_found".localized
            }
        }
    }
    
}

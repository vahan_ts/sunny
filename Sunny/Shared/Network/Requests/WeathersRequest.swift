//
//  WeathersRequest.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

enum WeathersRequest {
    case cities([String])
}

extension WeathersRequest: IWeatherDecodableRequest {
    
    typealias DecodableType = WeatherModels
    
    var path: String {
        return "/group"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .cities(let ids):
            return ["id": ids.joined(separator: ","), "units": "metric"]
        }
    }
    
    var encoding: RequestEncoding {
        return .query
    }
    
}

//
//  IRequest.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/12/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
}

enum RequestEncoding {
    case `default`
    case query
}

protocol IRequest {
    var path: String { get }
    var method: HTTPMethod { get }
    var headers: [String: String]? { get }
    var parameters: [String: Any]? { get }
    var encoding: RequestEncoding { get }
    var enviorment: IEnviorment { get }
}

protocol IDecodableRequest: IRequest {
    associatedtype DecodableType: Decodable
}

protocol IWeatherRequest: IRequest {
}

extension IWeatherRequest {
    
    var enviorment: IEnviorment {
        return AppConstants.Enviorment.current
    }
    
}

typealias IWeatherDecodableRequest = IWeatherRequest & IDecodableRequest

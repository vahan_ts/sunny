//
//  WeatherRequest.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

enum WeatherRequest {
    case city(name: String)
    case location(latitude: Double, longitude: Double)
    case cityID(id: Int)
}

extension WeatherRequest: IWeatherDecodableRequest {
    
    typealias DecodableType = WeatherModel
    
    var path: String {
        return "weather"
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: [String : String]? {
        return nil
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .city(let name):
            return ["q": name, "units": "metric"]
        case .location(let latitude, let longitude):
            return ["lat": latitude, "lon": longitude, "units": "metric"]
        case .cityID(let id):
            return ["id": id, "units": "metric"]
        }
    }
    
    var encoding: RequestEncoding {
        return .query
    }
    
}

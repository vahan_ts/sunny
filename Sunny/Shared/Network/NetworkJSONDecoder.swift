//
//  NetworkJSONDecoder.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation


protocol IJSONDecoder {
    
    func decodeData<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable
    
}


extension JSONDecoder: IJSONDecoder {
    
    func decodeData<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        return try self.decode(type, from: data)
    }
    
}

class NetworkJSONDecoder: IJSONDecoder {
    
    lazy var decoder: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }()
    
    func decodeData<T>(_ type: T.Type, from data: Data) throws -> T where T : Decodable {
        return try self.decoder.decodeData(type, from: data)
    }
    
}

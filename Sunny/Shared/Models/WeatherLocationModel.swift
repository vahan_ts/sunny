//
//  WeatherLocation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/11/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

struct WeatherLocationModel {
    
    let name: String
    let id: Int
    let countryCode: String
    var weathers: [WeatherModel]
    var isMyLocation = false
    
    init(with location: WeatherLocationCDModel) throws {
        guard  let name = location.name, let weathers = location.weathers?.allObjects as? [WeatherCDModel] else {
            throw CoreDataError.incorrectData
        }
        self.id = Int(location.id)
        self.name = name
        self.countryCode = location.countryCode ?? ""
        self.weathers = try weathers.map({ (weather) -> WeatherModel in
            return try WeatherModel(with: weather)
        })
        
        self.weathers.sort { (m1, m2) -> Bool in
            return m1.date > m2.date
        }
    }
    
    init(with weather: WeatherModel) {
        self.id = weather.id
        self.name = weather.cityName
        self.weathers = [weather]
        self.countryCode = weather.sys.country
    }
        
}

extension WeatherLocationModel {
    
    mutating func addIfNeeded(weather: WeatherModel) -> Bool {
        guard weather.id == self.id else {
            return false
        }
        let minFiltrSeconds = AppConstants.weatherHistoryMinTime
        if let firstWeather = self.weathers.first, (weather.date.timeIntervalSince(firstWeather.date)) >= minFiltrSeconds {
            self.weathers.insert(weather, at: 0)
            return true
        } else if self.weathers.count == 0 {
            self.weathers.append(weather)
            return true
        }
        return false
    }
    
    var isUpToDate: Bool {
        guard let weather = self.weathers.first else {
            return false
        }
        return (Date().timeIntervalSince(weather.date)) < AppConstants.weatherHistoryMinTime
    }
    
    
    static func colorSYS(for weatherLocationModel: WeatherLocationModel?) -> UIColor {
        guard let weather = weatherLocationModel?.weathers.first else {
            return .day
        }
        if weather.date < weather.sys.sunrise {
            return .night
        }
        return .day
    }
    
}

//
//  WeatherDBModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/10/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import CoreData


extension WeatherInfoCDModel {
        
    convenience init(with info: WeatherModel.Info, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        self.temperature = info.temperature
        self.humidity = info.humidity
        self.pressure = info.pressure
        self.minTemp = info.minTemp
        self.maxTemp = info.maxTemp
    }
    
}

extension WeatherWindCDModel {
    
    convenience init(with wind: WeatherModel.Wind, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        self.speed = wind.speed
        self.deg = wind.deg
    }
    
}


extension WeatherCoordinatsCDModel {
    
    convenience init(with coordinats: WeatherModel.Coordinats, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        self.longitude = coordinats.longitude
        self.latitude = coordinats.latitude
    }
    
}

extension WeatherDescriptionCDModel {
    
    convenience init(with descriptionInfo: WeatherModel.DescriptionInfo, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        
        self.id = Int64(descriptionInfo.id)
        self.main = descriptionInfo.main
        self.descriptionInfo = descriptionInfo.descriptionInfo
        self.icon = descriptionInfo.icon
    }
}

extension WeatherSYSCDModel {

    convenience init(with sys: WeatherModel.SYS, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)

        self.country = sys.country
        self.sunrise = sys.sunrise
        self.sunset = sys.sunset
    }

}

extension WeatherCDModel {

    convenience init(with weather: WeatherModel, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        
        self.id = Int64(weather.id)
        self.cityName = weather.cityName
        self.date = weather.date
        self.visibility = Int32(weather.visibility)
        self.coordinats = WeatherCoordinatsCDModel(with: weather.coordinats, insertInto: context)
        self.info = WeatherInfoCDModel(with: weather.info, insertInto: context)
        self.wind = WeatherWindCDModel(with: weather.wind, insertInto: context)
        self.sys = WeatherSYSCDModel(with: weather.sys, insertInto: context)

        let descriptionInfos = weather.descriptionInfos.compactMap({ (info) -> WeatherDescriptionCDModel in
            return WeatherDescriptionCDModel(with: info, insertInto: context)
        })
        self.descriptionInfos = NSSet(array: descriptionInfos)
    }
    
}


//
//  WeatherLocationCDModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/11/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import CoreData

extension WeatherLocationCDModel {
    
    convenience init(with location: WeatherLocationModel, insertInto context: NSManagedObjectContext? = nil) {
        let description = Self.entity()
        self.init(entity: description, insertInto: context)
        self.update(with: location)
    }
    
    func update(with location: WeatherLocationModel) {
        self.id = Int64(location.id)
        self.name = location.name
        self.countryCode = location.countryCode
        let weathers = location.weathers.map({ (weather) -> WeatherCDModel in
            return WeatherCDModel(with: weather, insertInto: self.managedObjectContext)
        })
        self.weathers = NSSet(array: weathers)
    }
    
    func addToWeathers(_ value: WeatherModel) {
        if self.weathers == nil {
            self.weathers = NSSet()
        }
        let cdModel = WeatherCDModel(with: value, insertInto: self.managedObjectContext)
        self.addToWeathers(cdModel)

    }
    
    
}

//
//  WeatherLocationsMode.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

struct WeatherModels: Decodable {
   
    let count: Int
    let weatherLocations: [WeatherModel]
    
    private enum CodingKeys: String, CodingKey {
        case count = "cnt"
        case weatherLocations = "list"
    }
    
}

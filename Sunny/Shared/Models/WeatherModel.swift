//
//  WeatherModel.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/10/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

extension WeatherModel {

    struct Coordinats: Decodable {

        private enum CodingKeys: String, CodingKey {
            case longitude = "lon"
            case latitude = "lat"
        }

        let longitude: Double
        let latitude: Double
        
        init(with weather: WeatherCoordinatsCDModel) {
            self.longitude = weather.longitude
            self.latitude = weather.latitude
        }
    }

    struct Wind: Decodable {

        private enum CodingKeys: String, CodingKey {
            case speed = "speed"
            case deg = "deg"
        }

        let speed: Double
        let deg: Double
        
        init(with wind: WeatherWindCDModel) {
            self.speed = wind.speed
            self.deg = wind.deg
        }
    }

    struct Info: Decodable {

        private enum CodingKeys: String, CodingKey {
            case temperature = "temp"
            case humidity = "humidity"
            case pressure = "pressure"
            case minTemp = "temp_min"
            case maxTemp = "temp_max"
        }

        let temperature: Double
        let humidity: Double
        let pressure: Double
        let minTemp: Double
        let maxTemp: Double
        
        init(with info: WeatherInfoCDModel) {
            self.temperature = info.temperature
            self.humidity = info.humidity
            self.pressure = info.pressure
            self.minTemp = info.minTemp
            self.maxTemp = info.maxTemp
        }

    }
    
    struct DescriptionInfo: Decodable {
        private enum CodingKeys: String, CodingKey {
            case id = "id"
            case main = "main"
            case descriptionInfo = "description"
            case icon = "icon"
        }
        
        let id: Int
        let main: String?
        let descriptionInfo: String?
        let icon: String?
        
        init(with descriptionInfo: WeatherDescriptionCDModel) {
            self.id = Int(descriptionInfo.id)
            self.main = descriptionInfo.main
            self.descriptionInfo = descriptionInfo.descriptionInfo
            self.icon = descriptionInfo.icon
        }
    }
    
    struct SYS: Decodable {
        
        private enum CodingKeys: String, CodingKey {
            case country = "country"
            case sunrise = "sunrise"
            case sunset = "sunset"
        }
        
        let country: String
        let sunrise: Date
        let sunset: Date
        
        init(with sys: WeatherSYSCDModel) throws {
            guard let country = sys.country, let sunrise = sys.sunrise, let sunset = sys.sunset else {
                throw CoreDataError.incorrectData
            }
            self.country = country
            self.sunrise = sunrise
            self.sunset = sunset
        }
        
    }

}

struct WeatherModel: Decodable {

    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case cityName = "name"
        case date = "dt"
        case sys = "sys"
        case coordinats = "coord"
        case wind = "wind"
        case info = "main"
        case visibility = "visibility"
        case descriptionInfos = "weather"
    }

    let id: Int
    let cityName: String
    let date: Date
    let sys: SYS
    let coordinats: Coordinats
    let wind: Wind
    let info: Info
    let descriptionInfos: [DescriptionInfo]
    let visibility: Int
    
    init(with weather: WeatherCDModel) throws {
        
        guard let date = weather.date, let coordinats = weather.coordinats, let info = weather.info, let cityName = weather.cityName, let wind = weather.wind, let descriptionInfos = weather.descriptionInfos?.allObjects as? [WeatherDescriptionCDModel], let sys = weather.sys else {
            throw CoreDataError.incorrectData
        }
        
        self.sys = try SYS(with: sys)
        self.id = Int(weather.id)
        self.cityName = cityName
        self.date = date
        self.visibility = Int(weather.visibility)
        self.coordinats = Coordinats(with: coordinats)
        self.info = Info(with: info)
        self.wind = Wind(with: wind)
                
        self.descriptionInfos = descriptionInfos.compactMap({ (model) -> DescriptionInfo in
            return DescriptionInfo(with: model)
        })
    }

}

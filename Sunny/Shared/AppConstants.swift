//
//  AppConstants.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

struct AppConstants {
    
    struct Enviorment: IEnviorment {
        var baseUrl: String
        var appID: String
        var imageUrl: String
      
        var parameters: [String : Any]? {
            return ["appid": self.appID]
        }
        
        static let current = Enviorment(
            baseUrl: "http://api.openweathermap.org/data/2.5/",
            appID: "4e41c354247b9bff4a9fa26f51307ec7",
            imageUrl: "http://openweathermap.org/img/wn/")
    }
    
    static let locationDistanceFilter: Double = 1000
    static let weatherHistoryMinTime: TimeInterval = 3600
    
}

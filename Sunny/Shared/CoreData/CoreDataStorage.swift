//
//  CoreDataStorage.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/10/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import CoreData

enum CoreDataError: WeatherAppError {
    
    case incorrectData
    case coreDataFailed(Error)
    case notFound
    
    var message: String {
        switch self {
        case .incorrectData:
            return "error_incorrect_data".localized
        case .coreDataFailed(let error):
            return error.localizedDescription
        case .notFound:
            return "error_not_found".localized
        }
    }
}

class CoreDataStorage {
        
    let dbName: String
    
    var context: NSManagedObjectContext {
        return self.persistentContainer.viewContext
    }
    
    init(with dbName: String) {
        self.dbName = dbName
    }
    
    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: self.dbName)
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext() {
        let context = self.persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    func executeCoreDataRequest(_ executeBlock: @escaping () -> Void) {
        self.context.performAndWait({
            executeBlock()
        })
    }

    func executeCoreDataRequestReturningObject<T>(_ executeBlock: @escaping () -> T?) -> T? {
        var result: T?
        self.context.performAndWait({
            result = executeBlock()
        })
        return result
    }
    
}

extension NSManagedObject {

    class func entityName() -> String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
    
}

//
//  WeatherCoreDataHelper.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import CoreData

class WeatherCoreDataHelper {
    
    let dataStorage = CoreDataStorage(with: "Weather")
    
    var context: NSManagedObjectContext {
        return self.dataStorage.context
    }
    
    //MARK: - Public methods
    
    func saveContext() {
        self.dataStorage.saveContext()
    }
    
    func weatherLocationCDModel(by id: Int) -> Result<WeatherLocationCDModel, Error>? {
        return self.dataStorage.executeCoreDataRequestReturningObject { () -> Result<WeatherLocationCDModel, Error>? in
            do {
                if let result = try self.getWeatherLocationCDModel(by: id) {
                    return .success(result)
                }
                return nil
            } catch {
                return  .error(error)
            }
        }
    }

    func weatherLocationCDModels() -> Result<[WeatherLocationCDModel], Error> {
        return self.dataStorage.executeCoreDataRequestReturningObject { () -> Result<[WeatherLocationCDModel], Error>? in
            do {
                let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: WeatherLocationCDModel.entityName())
                let fetchResults = try self.context.fetch(fetchRequest) as! [WeatherLocationCDModel]
                return .success(fetchResults)
            } catch {
                return  .error(error)
            }
        }!
    }
        
    //MARK: Private methods
    
    private func delete(weatherLocationCDModel model: WeatherLocationCDModel) {
        self.dataStorage.executeCoreDataRequest {
            self.context.delete(model)
        }
    }
    
    private func getWeatherLocationCDModel(by id: Int) throws -> WeatherLocationCDModel? {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: WeatherLocationCDModel.entityName())
        let fetchResults = try self.context.fetch(fetchRequest) as! [WeatherLocationCDModel]
        return fetchResults.first
    }
    
    private func createWeatherLocationCDModel(from model: WeatherLocationModel) -> WeatherLocationCDModel {
        let cdModel = WeatherLocationCDModel(with: model, insertInto: self.context)
        return cdModel
    }
    
}

extension WeatherCoreDataHelper {
    
    @discardableResult
    func createSaveWeatherLocationCDModel(from models: [WeatherLocationModel]) -> [WeatherLocationCDModel] {
        return self.dataStorage.executeCoreDataRequestReturningObject { () -> [WeatherLocationCDModel] in
            var cdModels = [WeatherLocationCDModel]()
            models.forEach { (model) in
                let cdModel = self.createWeatherLocationCDModel(from: model)
                cdModels.append(cdModel)
            }
            self.saveContext()
            return cdModels
        }!
    }
    
    @discardableResult
    func updateWeatherLocationCDModel(from models: [WeatherLocationModel]) -> [WeatherLocationCDModel] {
        return self.dataStorage.executeCoreDataRequestReturningObject { () -> [WeatherLocationCDModel] in
            var cdModels = [WeatherLocationCDModel]()
            models.forEach { (model) in
                if let cdModel = try? self.getWeatherLocationCDModel(by: model.id) {
                    cdModel.update(with: model)
                    cdModels.append(cdModel)
                }
            }
            self.saveContext()
            return cdModels
        }!
    }
    
    func delete(weatherLocationCDModel modelID: Int) -> Result<Bool, Error> {
        return self.dataStorage.executeCoreDataRequestReturningObject { () -> Result<Bool, Error>? in
            do {
                guard let fetchResults = try self.getWeatherLocationCDModel(by: modelID) else {
                    return .error(CoreDataError.notFound)
                }
                self.context.delete(fetchResults)
                self.saveContext()
                return .success(true)
            } catch {
                return  .error(error)
            }
        }!
    }
    
}

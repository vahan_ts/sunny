//
//  DateManager.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

class DateManager {
    
    private let dateFormatter: DateFormatter
    
    init(timeZone: String) {
        self.dateFormatter = DateFormatter()
        self.dateFormatter.timeZone = TimeZone(abbreviation: timeZone)
    }
    
    func dateToString(date: Date, withFormate format: DateFormat) -> String {
        self.dateFormatter.dateFormat = format.rawValue
        return self.dateFormatter.string(from: date)
    }

    func dateToString(date: Date, format: String) -> String {
        self.dateFormatter.dateFormat = format
        return self.dateFormatter.string(from: date)
    }
    
    func stringToDate(dateString: String, format: DateFormat) -> Date? {
        self.dateFormatter.dateFormat = format.rawValue
        let date = self.dateFormatter.date(from: dateString)
        return date
    }
    
}

extension DateManager {
    
    enum DateFormat: String {
        case YYYY_MM_DD_HH = "YY/MM/dd HH"
    }

}

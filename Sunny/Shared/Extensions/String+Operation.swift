//
//  String+Operation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/11/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

extension String {
        
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
   
}

//
//  Error+Operation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol WeatherAppError: LocalizedError {
    var message: String { get }
}

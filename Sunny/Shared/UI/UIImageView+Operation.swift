//
//  UIImageView+Operation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/14/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit
import Kingfisher

extension UIImageView {
    
    func setImage(imageUrlStr: String?, placeholder: UIImage? = nil) {
        if let imageUrlStr = imageUrlStr {
            let url = URL(string: imageUrlStr)
            self.setImage(url: url, placeholder: placeholder)
        } else {
            let url: URL? = nil
            self.setImage(url: url, placeholder: placeholder)
        }
    }
    
    func setImage(url: URL?, placeholder: UIImage? = nil) {
        self.kf.setImage(with: url, placeholder: placeholder, options: [.transition(.fade(0.1))])
    }
    
}

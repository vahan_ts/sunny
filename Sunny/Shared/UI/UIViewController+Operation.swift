//
//  UIViewController+Operation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/14/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit

extension UIViewController {
    
    struct AlertModel {
        var title: String? = nil
        var message: String? = nil
        var actions: [Action]? = nil
        
        struct Action {
            let title: String
            let style: Style
            var block: (() -> Void)? = nil
                        
            enum Style {
                case `default`
                case cancel
                var alertActionStyle: UIAlertAction.Style {
                    switch self {
                    case .default:
                        return .default
                    case .cancel:
                        return .cancel
                    }
                }
            }
        }
        
        mutating func addAction(action: Action) {
            if self.actions == nil {
                self.actions = [action]
            } else {
                self.actions?.append(action)
            }
        }
        
    }
    
    func display(_ alert: WeatherViewController.AlertModel) {
        let alertController = UIAlertController(title: alert.title, message: alert.message, preferredStyle: .alert)
        alert.actions?.forEach({ (action) in
            let alertAction = UIAlertAction(title: action.title, style: action.style.alertActionStyle) { (_) in
                action.block?()
            }
            alertController.addAction(alertAction)
        })
        self.present(alertController, animated: true)
    }
    
}

//
//  UIColor+Operation.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit


extension UIColor {
    
    class var day: UIColor {
        return UIColor(named: "Day") ?? UIColor.black
    }
    
    class var night: UIColor {
        return UIColor(named: "Night") ?? UIColor.black
    }
    
}

//
//  IWeatherService.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/16/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import Foundation

protocol IWeatherService {
    
    func addObserver(obj: IWeatherServiceObserver)
    func removeObserver(obj: IWeatherServiceObserver)
    
    func requestWeather(name: String,
                        success: @escaping (WeatherLocationModel) -> Void,
                        failure: @escaping (Error) -> Void)
    
    func requestWeather(longitude: Double,
                        latitude: Double,
                        success: @escaping (WeatherLocationModel) -> Void,
                        failure: @escaping (Error) -> Void)
    
    func requestWeather(cityID: Int,
                        success: @escaping (WeatherLocationModel) -> Void,
                        failure: @escaping (Error) -> Void)
    
    
    var currentLocationStatus: WeatherService.CurrentLocationStatus? { get }
    
    
    func weatherLocationModel(by id: Int) -> WeatherLocationModel?
    
    func add(_ weatherLocationModel: WeatherLocationModel)
    
    var locationModels: [WeatherLocationModel] { get }
    
    var isReady: Bool { get }
    
    func delete(by locationModelID: Int)
    
}

protocol IWeatherServiceObserver {
    func didUpdate(service: IWeatherService, weatherLocation models: [WeatherLocationModel])
    func didAdd(service: IWeatherService, weatherLocation models: [WeatherLocationModel])
    func didChange(service: IWeatherService, currentLocation status: WeatherService.CurrentLocationStatus)
    
    
    func didDelete(service: IWeatherService, weatherLocation models: [WeatherLocationModel])
    
    func didReady(service: IWeatherService)
}

extension IWeatherServiceObserver {
    
    func didUpdate(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {}
    func didAdd(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {}
    func didChange(service: IWeatherService, currentLocation status: WeatherService.CurrentLocationStatus) {}
    func didDelete(service: IWeatherService, weatherLocation models: [WeatherLocationModel]) {}
    
    func didReady(service: IWeatherService) {}
}


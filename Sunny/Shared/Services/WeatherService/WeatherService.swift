//
//  WeatherService.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/13/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import UIKit


class WeatherService {
    
    typealias CurrentLocationStatus = Result<Int, Error>
        
    private static let service = WeatherService()
    private let observer = Observer<IWeatherServiceObserver>()
    private let coreDataHelper = WeatherCoreDataHelper()
    private let locationService = LocationService()
    private let dispatcher = NetworkDispatcher()
    private weak var lastUpdateAllDataSessionTask: URLSessionDataTask?
    private var lastUpdateAllDate: Date?
    private var isConfigured = false
    
    private let backgroundTasksIdentifier = "com.polidea.sunny.sunny.db_working"
    private var backgroundProcessingTask: Any?
    
    private(set) var locationModels = [WeatherLocationModel]()
    private(set) var currentLocationStatus: CurrentLocationStatus?
    private(set) var isReady = false
    
    static var weatherService: IWeatherService {
        return self.service
    }
        
    private init() {
        self.setup()
    }
    
    class func configure() {
        guard !Self.service.isConfigured else {
            return
        }
        Self.service.isConfigured = true
        if #available(iOS 13.0, *) {
            Self.service.enableBackgroundTasks()
        }
    }
    
    private func addActiveNotification() {
        NotificationCenter.default.addObserver(forName: UIApplication.didBecomeActiveNotification, object: nil, queue: nil) {[weak self] (_) in
            self?.setup()
        }
    }
    
    private func removeActiveNotification() {
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    private func setup() {
        guard !self.isReady else {
            return
        }
        guard UIApplication.shared.applicationState == .active else {
            self.addActiveNotification()
            return
        }
        self.isReady = true
        self.removeActiveNotification()
        self.setupDatabase()
        self.setupLocationService()
        self.setupTimer()
        self.didReady()
    }
    
    private func setupDatabase()  {
        let locationsResult = self.coreDataHelper.weatherLocationCDModels()
        switch locationsResult {
        case .success(let weatherCDLocations):
            self.generateLocationModels(weatherCDLocations: weatherCDLocations)
            self.updateAll()
        case .error(let error):
            print("error: ", error)
        }
    }
    
    private func generateLocationModels(weatherCDLocations: [WeatherLocationCDModel]) {
        self.locationModels.removeAll()
        for cdModel in weatherCDLocations {
            guard let model = try? WeatherLocationModel(with: cdModel) else {
                continue
            }
            self.locationModels.append(model)
        }
    }
    
    private func reloadCurrentLocation() {
        guard let status = self.currentLocationStatus else {
            return
        }
        var myLocationID: Int?
        switch status {
        case .success(let id):
            myLocationID = id
        case .error(_):
            break
        }
        guard let id = myLocationID else {
            return
        }
        var locationModels = [WeatherLocationModel]()
        self.locationModels.forEach { (model) in
            var model = model
            model.isMyLocation = model.id == id
            locationModels.append(model)
        }
        self.locationModels = locationModels
    }
    
}


//Timer
extension WeatherService {
        
    private func setupTimer() {
        _ = Timer.scheduledTimer(withTimeInterval: AppConstants.weatherHistoryMinTime, repeats: true, block: {[weak self] (timer) in
            self?.updateAll()
        })
    }
    
    private func updateAll(completed: ((Bool) -> Void)? = nil) {
        var ids = [String]()
        self.locationModels.forEach { (model) in
            if !model.isUpToDate {
                ids.append("\(model.id)")
            }
        }
        guard ids.count > 0 else {
            completed?(true)
            return
        }
        self.lastUpdateAllDataSessionTask = self.requestWeathers(cityIDs: ids, saveDB: true) {[weak self] (_) in
            self?.lastUpdateAllDate = Date()
            completed?(true)
        } failure: {(_) in
            completed?(true)
        }
    }
    
}

//LocationService
extension WeatherService {
    
    private func setupLocationService() {
        self.locationService.enable {[weak self] (result) in
            switch result {
            case .success(let location):
                let coordinate = location.coordinate
                self?.updateCurrentLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
            case .error(let error):
                let status = CurrentLocationStatus.error(error)
                self?.currentLocationStatus = status
                self?.didChangedCurrentLocationStatus(status)
            }
        }
    }
    
    private func updateCurrentLocation(latitude: Double, longitude: Double) {
        self.requestWeather(longitude: latitude, latitude: longitude, saveDB: true) {[weak self] (weatherLocationModel) in
            let status = CurrentLocationStatus.success(weatherLocationModel.id)
            self?.currentLocationStatus = status
            self?.didChangedCurrentLocationStatus(status)
        } failure: {[weak self] (error) in
            let status = CurrentLocationStatus.error(error)
            self?.currentLocationStatus = status
        }
    }
    
}

//Core data
extension WeatherService {
    
    private func creatOrUpdateIfNeededWeatherLocationModel(for weatherModel: WeatherModel, saveDB: Bool) -> WeatherLocationModel {
        if let locationModelIndex = self.locationModels.firstIndex(where: { $0.id == weatherModel.id }) {
            let isAdded = self.locationModels[locationModelIndex].addIfNeeded(weather: weatherModel)
            if isAdded {
                if saveDB {
                    self.coreDataHelper.updateWeatherLocationCDModel(from: [self.locationModels[locationModelIndex]])
                }
                self.didUpdate(weatherLocation: [self.locationModels[locationModelIndex]])
            }
            return self.locationModels[locationModelIndex]
        }
        let locationModel = WeatherLocationModel(with: weatherModel)
        if saveDB {
            self.coreDataHelper.createSaveWeatherLocationCDModel(from: [locationModel])
            self.locationModels.append(locationModel)
            self.didAdde(weatherLocation: [locationModel])
        }
        return locationModel
    }
    
    private func creatOrUpdateIfNeededWeatherLocationModels(for weatherModels: [WeatherModel], saveDB: Bool) -> [WeatherLocationModel] {
        var createdLocationModels = [WeatherLocationModel]()
        var updatedLocationModels = [WeatherLocationModel]()
        var result = [WeatherLocationModel]()
        
        weatherModels.forEach { (weatherModel) in
            if let locationModelIndex = self.locationModels.firstIndex(where: { $0.id == weatherModel.id }) {
                if self.locationModels[locationModelIndex].addIfNeeded(weather: weatherModel) {
                    updatedLocationModels.append(self.locationModels[locationModelIndex])
                    result.append(self.locationModels[locationModelIndex])
                }
            } else {
                let locationModel = WeatherLocationModel(with: weatherModel)
                createdLocationModels.append(locationModel)
                result.append(locationModel)
            }
        }
        
        if updatedLocationModels.count > 0 {
            self.coreDataHelper.updateWeatherLocationCDModel(from: updatedLocationModels)
            self.didUpdate(weatherLocation: updatedLocationModels)
        }
        
        if saveDB {
            if createdLocationModels.count > 0 {
                self.locationModels.append(contentsOf: createdLocationModels)
                self.coreDataHelper.createSaveWeatherLocationCDModel(from: createdLocationModels)
                self.didAdde(weatherLocation: createdLocationModels)
            }
        }
        
        return result
    }
    
}

//Request
extension WeatherService {
    
    @discardableResult
    private func requestWeather(request: WeatherRequest, saveDB: Bool, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) -> URLSessionDataTask? {
        return self.dispatcher.request(request: request, success: { (weather) in
            let locationModel = Self.service.creatOrUpdateIfNeededWeatherLocationModel(for: weather, saveDB: saveDB)
            success(locationModel)
        }, failure: failure)
    }
    
    @discardableResult
    private func requestWeather(name: String, saveDB: Bool, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) -> URLSessionDataTask? {
        let request = WeatherRequest.city(name: name)
        return self.requestWeather(request: request, saveDB: saveDB, success: success, failure: failure)
    }
    
    @discardableResult
    private func requestWeather(longitude: Double, latitude: Double, saveDB: Bool, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) -> URLSessionDataTask? {
        let request = WeatherRequest.location(latitude: longitude, longitude: latitude)
        return self.requestWeather(request: request, saveDB: saveDB, success: success, failure: failure)
    }
    
    @discardableResult
    private func requestWeather(cityID: Int, saveDB: Bool, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) -> URLSessionDataTask? {
        let request = WeatherRequest.cityID(id: cityID)
        return self.requestWeather(request: request, saveDB: saveDB, success: success, failure: failure)
    }
    
    @discardableResult
    private func requestWeathers(cityIDs: [String], saveDB: Bool, success: @escaping ([WeatherLocationModel]) -> Void, failure: @escaping (Error) -> Void) -> URLSessionDataTask?  {
        let request = WeathersRequest.cities(cityIDs)
        return self.dispatcher.request(request: request, success: { (weathers) in
            let result = Self.service.creatOrUpdateIfNeededWeatherLocationModels(for: weathers.weatherLocations, saveDB: saveDB)
            success(result)
        }, failure: failure)
    }
    
}

extension WeatherService {
    
    private func didUpdate(weatherLocation models: [WeatherLocationModel]) {
        self.reloadCurrentLocation()
        self.observer.forEach { (obj) in
            obj.didUpdate(service: self, weatherLocation: models)
        }
    }
    
    private func didAdde(weatherLocation models: [WeatherLocationModel]) {
        self.observer.forEach { (obj) in
            obj.didAdd(service: self, weatherLocation: models)
        }
    }
    
    private func didDelete(weatherLocation models: [WeatherLocationModel]) {
        self.observer.forEach { (obj) in
            obj.didDelete(service: self, weatherLocation: models)
        }
    }
        
    private func didChangedCurrentLocationStatus(_ status: CurrentLocationStatus) {
        self.reloadCurrentLocation()
        self.observer.forEach { (obj) in
            obj.didChange(service: self, currentLocation: status)
        }
    }
    
    private func didReady() {
        self.observer.forEach { (obj) in
            obj.didReady(service: self)
        }
    }
    
}

extension WeatherService: IWeatherService {
    
    func addObserver(obj: IWeatherServiceObserver) {
        self.observer.addObject(obj)
    }
    
    func removeObserver(obj: IWeatherServiceObserver) {
        self.observer.removeObject(obj)
    }
    
    func requestWeather(name: String, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) {
        let request = WeatherRequest.city(name: name)
        self.requestWeather(request: request, saveDB: false, success: success, failure: failure)
    }
    
    func requestWeather(longitude: Double, latitude: Double, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) {
        let request = WeatherRequest.location(latitude: longitude, longitude: latitude)
        self.requestWeather(request: request, saveDB: false, success: success, failure: failure)
    }
    
    func requestWeather(cityID: Int, success: @escaping (WeatherLocationModel) -> Void, failure: @escaping (Error) -> Void) {
        if let model = self.locationModels.first(where: { $0.id == cityID }) {
            success(model)
            return
        }
        let request = WeatherRequest.cityID(id: cityID)
        self.requestWeather(request: request, saveDB: false, success: success, failure: failure)
    }
    
    func weatherLocationModel(by id: Int) -> WeatherLocationModel? {
        return self.locationModels.first(where: { $0.id == id})
    }
    
    func add(_ weatherLocationModel: WeatherLocationModel) {
        guard self.weatherLocationModel(by: weatherLocationModel.id) == nil else {
            return
        }
        self.coreDataHelper.createSaveWeatherLocationCDModel(from: [weatherLocationModel])
        self.locationModels.append(weatherLocationModel)
        self.didAdde(weatherLocation: [weatherLocationModel])
    }
    
    func delete(by locationModelID: Int) {
        let delete = self.coreDataHelper.delete(weatherLocationCDModel: locationModelID)
        var deleteModels = [WeatherLocationModel]()
        switch delete {
        case .success(let end):
            if end {
                self.locationModels.removeAll { (model) -> Bool in
                    if model.id == locationModelID {
                        deleteModels.append(model)
                        return true
                    }
                    return false
                }
            }
        case .error(_):
            break
        }
        if deleteModels.count > 0  {
            self.didDelete(weatherLocation: deleteModels)
        }
    }
    
}

@available(iOS 13.0, *)
extension WeatherService {
    
    private func enableBackgroundTasks() {
        let backgroundProcessingTask = BackgroundRefreshTaskTasks(forTaskWithIdentifier:  self.backgroundTasksIdentifier, using: nil) {[weak self] (tasks) in
            
            try? tasks.submit()
            self?.updateAll(completed: { (end) in
                tasks.setTaskCompleted(success: end)
            })
            
            tasks.expirationHandler = { [weak self] in
                self?.lastUpdateAllDataSessionTask?.cancel()
            }
            
        } submit: {[weak self] (tasks) -> BackgroundRefreshTaskTasks.SubmitParams in
            let timeInterval = AppConstants.weatherHistoryMinTime
            let date = self?.lastUpdateAllDate?.addingTimeInterval(timeInterval) ?? Date(timeIntervalSinceNow: 10)
            return BackgroundRefreshTaskTasks.SubmitParams(true, date)
        }
        
        self.backgroundProcessingTask = backgroundProcessingTask

    }
    
}


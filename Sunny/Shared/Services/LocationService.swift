//
//  LocationService.swift
//  Sunny
//
//  Created by Polidea on 13/10/2017.
//  Copyright © 2017 Polidea. All rights reserved.
//

import CoreLocation
import UIKit

enum LocationServiceError: Error {
    case locationManagerFailed(Error)
    case unAuthorized
}

class LocationService: NSObject, CLLocationManagerDelegate {

    typealias LocationUpdateBlock = (Result<CLLocation, LocationServiceError>) -> Void
    private let distanceFilter: CLLocationDistance = AppConstants.locationDistanceFilter

    private(set) var lastLocation: CLLocation? {
        didSet {
            if let value = lastLocation {
                currentObserverBlock?(.success(value))
            }
        }
    }

    private var currentObserverBlock: LocationUpdateBlock?
    
    var isEnabled: Bool {
        return currentObserverBlock != nil
    }

    private lazy var locationManager: CLLocationManager = {
        let manager = CLLocationManager()
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.distanceFilter = distanceFilter
        manager.allowsBackgroundLocationUpdates = true
        manager.requestAlwaysAuthorization()
        return manager
    }()
    
    private func updateObserver() {
        if let lastLocation = lastLocation {
            currentObserverBlock?(.success(lastLocation))
        }
    }

    func enable(with block: @escaping LocationUpdateBlock) {
        currentObserverBlock = block
        locationManager.startUpdatingLocation()
        
        switch CLLocationManager.authorizationStatus() {
        case .denied:
            currentObserverBlock?(.error(.unAuthorized))
        default:
            break
        }
    }

    func disable() {
        currentObserverBlock = nil
        locationManager.stopUpdatingLocation()
    }

    func locationManager(_: CLLocationManager, didFailWithError error: Error) {
        currentObserverBlock?(.error(.locationManagerFailed(error)))
    }

    func locationManager(_: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let last = locations.last {
            if let current = lastLocation {
                let distance = current.distance(from: last)
                if distance > distanceFilter {
                    lastLocation = last
                }
            } else {
                lastLocation = last
            }
        }
    }
}

//
//  WeatherService+BGTask.swift
//  Sunny
//
//  Created by Vahan Tsogolakyan on 10/15/20.
//  Copyright © 2020 Kacper Harasim. All rights reserved.
//

import BackgroundTasks
import UIKit


@available(iOS 13.0, *)
class BackgroundTasks<T: BGTask, SubmitParams> {
    
    typealias SubmitBlock = (BackgroundTasks) -> SubmitParams
    fileprivate let submitBlock: SubmitBlock
    fileprivate var task: T?
    
    let identifier: String
    
    var expirationHandler: (() -> Void)?
    
    private init() {
        fatalError()
    }
    
    @discardableResult
    init(forTaskWithIdentifier identifier: String, using queue: DispatchQueue?, launchHandler: @escaping (BackgroundTasks) -> Void, submit: @escaping SubmitBlock) {
        self.submitBlock = submit
        self.identifier = identifier
        BGTaskScheduler.shared.register(forTaskWithIdentifier: identifier, using: queue) {[weak self] task in
            guard let weakSelf = self else {
                return
            }
            weakSelf.launchHandler(task: task as! T)
            launchHandler(weakSelf as! Self)
        }
        self.addNotification()
    }
    
    func setTaskCompleted(success: Bool) {
        self.task?.setTaskCompleted(success: success)
    }
    
    func submit() throws { }
    
    //MARK: - Private methods
    
    private func addNotification() {
        NotificationCenter.default.addObserver(forName: UIApplication.didEnterBackgroundNotification, object: nil, queue: nil) {[weak self] (_) in
            do {
                try self?.submit()
            } catch  {
                print(error)
            }
            try? self?.submit()
        }
    }
    
    fileprivate func launchHandler(task: T) {
        self.task = task
        task.expirationHandler = self.expirationHandler
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}


@available(iOS 13.0, *)
class BackgroundRefreshTaskTasks: BackgroundTasks<BGAppRefreshTask, BackgroundRefreshTaskTasks.SubmitParams> {
    
    typealias SubmitParams = (canSubmit: Bool, earliestBeginDate: Date?)
    
    override func submit() throws {
        try super.submit()
        let submitParams = self.submitBlock(self)
        guard submitParams.canSubmit else {
            return
        }
        
        let request = BGAppRefreshTaskRequest(identifier: self.identifier)
        request.earliestBeginDate = submitParams.earliestBeginDate
        try BGTaskScheduler.shared.submit(request)
    }
    

}

